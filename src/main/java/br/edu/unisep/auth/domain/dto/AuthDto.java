package br.edu.unisep.auth.domain.dto;

import lombok.Data;

@Data
public class AuthDto {

    private String login;

    private String password;

}
